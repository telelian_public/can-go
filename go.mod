module gitlab.com/telelian_public/can-go

go 1.13

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/fatih/color v1.13.0
	github.com/golang/mock v1.4.4-0.20200519145626-92f53b0a566c
	github.com/shurcooL/go-goon v0.0.0-20170922171312-37c2f522c041
	go.einride.tech/can v0.2.2
	go.uber.org/goleak v1.1.12
	golang.org/x/net v0.0.0-20211015210444-4f30a5c0130f
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
	golang.org/x/sys v0.0.0-20211019181941-9d821ace8654
	golang.org/x/tools v0.1.10
	gopkg.in/alecthomas/kingpin.v2 v2.2.6
	gotest.tools/v3 v3.1.0
)
